# Development
1. clone repo
2. run "npm install"
3. run "npm run start"

# Test production 
1. build image with "docker build -t {name}:{v#} ."
2. start image with "docker run -d -p 80:80 {image-name}:{v#}"

# Deployment
1. create tar file with "tar -czvf {name}-{v#}.tar.gz ."
2. upload file to caprover